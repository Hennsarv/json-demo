﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace JsonDemo
{
    class Program
    {
        static string FileName = @"..\..\Inimesed.json";
        static void Main(string[] args)
        {

            Console.Write("Loeme või kirjutame: ");
            switch (Console.ReadLine().ToLower())
            {
                case "test":
                    Inimene i1 = new Inimene { Nimi = "Jaak", Sünniaeg = DateTime.Today };
                    Inimene i2 = new Inimene("Ralf", DateTime.Today);

                    Console.WriteLine(i1);
                    Console.WriteLine(i2);
                    break;
                case "kirjutame":

                    Inimene h = new Inimene { Nimi = "Henn", Sünniaeg = new DateTime(1955, 03, 07) };
                    string j = JsonConvert.SerializeObject(h);
                    Console.WriteLine(j);

                    string a = "{'Nimi':'Ants','Sünniaeg':'1962-12-17'}";
                    Inimene u = JsonConvert.DeserializeObject<Inimene>(a);
                    Console.WriteLine(u);

                    List<Inimene> inimesed = new List<Inimene> { h, u };

                    string l = JsonConvert.SerializeObject(inimesed);

                    Console.WriteLine(l);
                    File.WriteAllText(FileName, l);
                    break;
                case "loeme":

                    string loe = File.ReadAllText(FileName);
                    Inimene[] inimesedArray = JsonConvert.DeserializeObject<Inimene[]>(loe);

                    foreach (var x in inimesedArray) Console.WriteLine(x);
                    break;
            }
        }
    }

    class Inimene
    {

        private bool test;

        public string Nimi;


        public DateTime Sünniaeg;

        // atribuudiga JsonIgnore saab tähistada fielde ja properteid,
        // mida ei ole vaja serialiseerida (tektikujule kaasata)
        [JsonIgnore]
        public int Vanus
        {
            get
            {
                int v = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(v) > DateTime.Today) v--;
                return v;
            }
        }

        public Inimene()
        {
            test = false;
        }

        // atribuudiga JsonConstructor saab tähistada, milllist konstruktorit
        // Json peab kasutama deserialiseerimisel (tekstiks tagasi teisendamisel)
        [JsonConstructor]
        public Inimene(string nimi, DateTime sünniaeg)
        {
            test = true;
            Nimi = nimi; Sünniaeg = sünniaeg;
        }

        
        public override string ToString()
        {
            return $"{Nimi} sündinud {Sünniaeg:dd.MMMM.yyyy} {(test ? "(J)" : "(N)")}";
        }
    }

}
